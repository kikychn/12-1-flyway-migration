create table if not exists contracts
(
    id        int auto_increment primary key,
    name      varchar(255) not null,
    client_id int          not null,
    foreign key (client_id)
        references clients (id)
);