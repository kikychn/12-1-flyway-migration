create table if not exists services
(
    id   int auto_increment primary key,
    type varchar(50) not null
);