create table if not exists offices
(
    id      int auto_increment primary key,
    country varchar(50) not null,
    city    varchar(50) not null
);