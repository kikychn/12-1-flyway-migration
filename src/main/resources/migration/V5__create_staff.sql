create table if not exists staff
(
    id         int auto_increment primary key,
    first_name varchar(30) not null,
    last_name  varchar(30) not null,
    office_id  int,
    foreign key (office_id)
        references offices (id)
);