create table if not exists contract_service_offices
(
    contract_id int,
    office_id   int,
    primary key (contract_id, office_id),
    foreign key (contract_id)
        references contracts (id),
    foreign key (office_id)
        references offices (id)
);